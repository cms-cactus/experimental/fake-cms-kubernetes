from flask import Flask, jsonify, request
from flask_restful import Api, Resource, reqparse

# Create a list of users containing their username, groups, and uid which will double as access token for this example
users = [
    {
        "username": "usamah",
        "groups": ["admin", "cms", "dev"],
        "uid": "11"
    },
    {
        "username": "glen",
        "groups": ["cms", "dev"],
        "uid": "12"
    },
    {
        "username": "alice",
        "groups": ["alice", "dev"],
        "uid": "13"
    },
    {
        "username": "bob",
        "groups": ["cms"],
        "uid": "14"
    }
]

# Create a dict of the users with their token (uid) as the key
tokens = {}
for user in users:
    tokens[user["uid"]] = user


class User(Resource):
    def get(self, name):
        for user in users:
            if name == user["username"] :
                return user, 200
        return "User not found", 404


# Create the handle for post requests that fills in the return as required by kubernetes
class Authenticate(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        result = {}
        status = {"authenticated": False}

        result["apiVersion"] = json_data["apiVersion"]
        result["kind"] = json_data["kind"]
        result["status"] = status        
        

        tk = json_data["spec"]["token"]
        if tk in tokens:
            status["authenticated"] = True
            status["user"] = tokens[tk]
            return jsonify(result)

        return jsonify(result)

app = Flask(__name__)
api = Api(app)
api.add_resource(User, "/user/<string:name>")
# Set the endpoint
api.add_resource(Authenticate,"/authenticate")

# ssl_context uses the certificates created to get certified
app.run(ssl_context=("../certs/domain.crt", "../certs/domain.key"), host="0.0.0.0")

