from flask import Flask, jsonify, request, redirect
from flask_restful import Api, Resource, reqparse
from flask_sslify import SSLify

from sanction import Client, transport_headers

from urllib.request import urlopen

import secrets

import sqlite3

from datetime import datetime

import json

TESTING = False

CLIENT_SECRET='Y2e1ulD9S1QOULu6n4ordBf0WRFRsO0hnI6PJsSonEU1'
CLIENT_ID='ujassat.kubauth'
TOKEN_ENDPOINT='https://oauth.web.cern.ch/OAuth/Token'

USER_ENDPOINT='https://oauthresource.web.cern.ch/api/User'
GROUPS_ENDPOINT='https://oauthresource.web.cern.ch/api/Groups'

DB_NAME = 'tokens.db'

client = Client(token_endpoint=TOKEN_ENDPOINT,
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET)

conn = sqlite3.connect(DB_NAME)
curs = conn.cursor()
curs.execute("CREATE TABLE IF NOT EXISTS tokens (kub_token text, access_token text, refresh_token text)")
conn.commit()
conn.close()


users = [
    {
        "username": "usamah",
        "groups": ["admin"],
        "uid": "11"
    },
    {
        "username": "glenn",
        "groups": ["ugmt", "dev", "cactus-cactus-admin"],
        "uid": "12"
    },
    {
        "username": "alice",
        "groups": ["ugmt", "dev", "test-k8s-deployment-groupA"],
        "uid": "13"
    },
    {
        "username": "bob",
        "groups": ["ugt", "test-k8s-deployment-groupB"],
        "uid": "14"
    }
]

tokens = {}
for user in users:
    tokens[user["uid"]] = user


class User(Resource):
    def get(self, name):
        for user in users:
            if name == user["username"] :
                return user, 200
        return "User not found", 404

class Test(Resource):
    def get(self):
        return jsonify("Testng string")

class OAuthHandle(Resource):
    def get(self):
        code = request.args.get('code', default="ERR")
        print("code: " + code)

        c = Client(token_endpoint='https://oauth.web.cern.ch/OAuth/Token',
            resource_endpoint='https://oauthresource.web.cern.ch/api/User',
            client_id='ujassat.kubauth',
            client_secret='Y2e1ulD9S1QOULu6n4ordBf0WRFRsO0hnI6PJsSonEU1')

        c.request_token(code=code,
            redirect_uri='https://vm-master:5000/handle_oauth')

        return "Your Kubernetes token is:" + c.access_token

ImageWhitelist = [
    "prometheus",
    "thanos",
    "nginx",
]

RepoWhitelist = [
    "calico",
    "k8s.gcr.io",
    "quay.io"
]

RepoAllowedList = [
    "vm-master:443"
]

class ImageCheck(Resource):
    def post(self):
        # Get and parse the request data
        json_data = request.get_json(force=True)

        print(json_data)

        result = {}
        result["apiVersion"] = json_data["apiVersion"]
        result["kind"] = json_data["kind"]
        status = {}
        status["allowed"] = False
        result["status"] = status


        # Allow everything when i'm testing the cluster
        if TESTING:
            status["allowed"] = True
            return jsonify(result)

        # Format Checks
        if json_data["kind"] != 'ImageReview':
            status["reason"] = 'Invalid kind field'
            return jsonify(result)

        if json_data["apiVersion"] != "imagepolicy.k8s.io/v1alpha1":
            # Different api Version needs implementing if there is
            status["reason"] = "apiVersion unsupported"
            return jsonify(results)

        for container in json_data["spec"]["containers"]:
            # Parse image name
            full_image_name = container["image"]
            parts = full_image_name.split("/")

            # No repo = unsafe
            if len(parts) == 1:
                status["reason"] = 'Image requires an explicit repo'
                return jsonify(result)

            # Get image name and repo
            image_name = parts[-1].split(":")[0].split("@")[0]
            repo = parts[0]

            if repo in RepoWhitelist:
                continue

            if repo in RepoAllowedList:
                if image_name in ImageWhitelist:
                    continue

                status["reason"] = "Image isn't allowed"
                return jsonify(result)

            status["reason"] = "Image repository is not allowed"
            return jsonify(result)

        status["allowed"] = True
        return jsonify(result)


class Authenticate(Resource):
    def post(self):
        # Open db in this thread
        conn = sqlite3.connect(DB_NAME)
        curs = conn.cursor()

        json_data = request.get_json(force=True)
        result = {}
        status = {"authenticated": False}

        result["apiVersion"] = json_data["apiVersion"]
        result["kind"] = json_data["kind"]
        result["status"] = status


        tk = json_data["spec"]["token"]
        if tk in tokens:
            status["authenticated"] = True
            status["user"] = tokens[tk]
            print(result)
            return jsonify(result)
        token = tk

        curs.execute("SELECT * FROM tokens WHERE kub_token = ?", (token,))
        res = curs.fetchone()


        if not res:
            return jsonify(result)

        client.access_token = res[1]
        client.refresh_token = res[2]


        user_req = transport_headers(USER_ENDPOINT, client.access_token)
        group_req = transport_headers(GROUPS_ENDPOINT, client.access_token)

        try:
            rep = urlopen(user_req)
            if rep.getcode() != 200:
                return jsonify(result)
            user_data = json.loads(rep.read().decode("utf-8"))


            rep = urlopen(group_req)
            if rep.getcode() != 200:
                return jsonify(result)
            group_data = json.loads(rep.read().decode("utf-8"))


            # Fill in the response if authenticated
            user = {}
            status["user"] = user
            user["username"] = user_data["username"]
            user["uid"] = str(user_data["id"])
            user["groups"] = group_data["groups"]

            status["authenticated"] = True

            # Refresh token as expires after 20 minutes
            client.refresh()

            # Update table
            curs.execute("UPDATE tokens SET access_token = ? , refresh_token = ? WHERE kub_token = ?;", (client.access_token, client.refresh_token, token))
            conn.commit()
            conn.close()

            return jsonify(result)
        except:
            print("Unauthorized access attempt")
            #print("kub_token: " + res[0])
            #print("acc_token: " + res[1])
            #print("ref_token: " + res[2])
            return jsonify(result)

        return jsonify(result)

app = Flask(__name__)
api = Api(app)
api.add_resource(Test, "/test")
#api.add_resource(OAuthHandle, "/handle_oauth")
api.add_resource(User, "/user/<string:name>")
api.add_resource(Authenticate,"/authenticate")
api.add_resource(ImageCheck, "/image_check")


@app.route("/handle_oauth")
def handleoauth():
    code = request.args.get('code', default="ERR")

    c = Client(token_endpoint='https://oauth.web.cern.ch/OAuth/Token',
        resource_endpoint='https://oauthresource.web.cern.ch/api/User',
        client_id='ujassat.kubauth',
        client_secret='Y2e1ulD9S1QOULu6n4ordBf0WRFRsO0hnI6PJsSonEU1')

    c.request_token(code=code,
        redirect_uri='https://vm-master:5000/handle_oauth')

    print("token expires: ", c.token_expires)
    print("token expires in: ", c.expires_in, " seconds")

    kub_token = secrets.token_urlsafe(64)
    access_token = c.access_token
    refresh_token = c.refresh_token

    conn = sqlite3.connect(DB_NAME)
    curs = conn.cursor()
    curs.execute("INSERT INTO tokens VALUES (?, ?, ?)", (kub_token, access_token, refresh_token))
    conn.commit()
    conn.close()

    expiration = datetime.fromtimestamp(c.token_expires)
    return "Your Kubernetes token is: </br>" + kub_token # + "</br> Expires: " + str(expiration)

@app.route("/get_token")
def gettoken():
    return redirect("https://oauth.web.cern.ch/OAuth/Authorize?response_type=code&redirect_uri=https://vm-master:5000/handle_oauth&client_id=ujassat.kubauth")

sslify = SSLify(app)
#app.run(ssl_context='adhoc')
app.run(ssl_context=("../certs/domain.crt", "../certs/domain.key"), host="0.0.0.0")
